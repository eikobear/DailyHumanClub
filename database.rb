
require "sequel"

class Database

  def self.instance
    @@instance ||= Database.new
  end

  def initialize
    Sequel::Model.plugin :timestamps
    @db = Sequel.connect(ENV["DATABASE_URL"])
    Dir[File.dirname(__FILE__) + "/models/*.rb"].each { |file| require file }
  end

  def db
    @db
  end

  def user_repo
  end

  def chat_repo
  end

  def chatter_repo
  end

  def message_repo
  end

  def chat_data chat_id
    { 
      participants: user_repo.by_chat(chat_id), 
      messages: message_repo.by_chat(chat_id),
    }
  end

end


__END__

class UserRepo
  commands :create, update: :by_pk, delete: :by_pk
  
  def find id
    users.where(id: id).one
  end

  def pick_searching user_id
    users.where(searching: true).exclude(id: user_id).order { random.function }.limit(1).one
  end

  def lookup email
    users.where(email: email).one
  end

  def make params
    changeset = changeset(CreateChangeset).data(params)
    create(changeset)
  end

  def live_chat id
    users.where(id: id).chats.where(live: true).one
  end
  
  def by_chat chat_id
    users.with_chatters.where(chat_id: chat_id).to_a
  end

end

class ChatRepo < ROM::Repository[:chats]
  commands :create, update: :by_pk, delete: :by_pk


  def make
    changeset = changeset(CreateChangeset).data({})
    create(changeset)
  end

end

class ChatterRepo < ROM::Repository[:chatters]
  commands :create, update: :by_pk, delete: :by_pk

  def make user_id, chat_id
    changeset = changeset(CreateChangeset).data(user_id: user_id, chat_id: chat_id)
    create(changeset)
  end

  def find chat_id
    chatters.where(chat_id: id).to_a
  end

  def other_than chat_id, user_id
    chatters.where(chat_id: chat_id).exclude(user_id: user_id)
  end

  def live_for_user user_id
    chatters.with_chats.where(live: true, user_id: user_id).one
  end

end

class MessageRepo < ROM::Repository[:messages]
  commands :create

  def make chatter_id, text
    changeset = changeset(CreateChangeset).data(chatter_id: chatter_id, text: text)
    create(changeset)
  end

  def by_chat chat_id
    messages.with_chatter.where(chat_id: chat_id).to_a
  end

end

class CreateChangeset < ROM::Changeset::Create
  map do |tuple|
    tuple.merge(created: Time.now, updated: Time.now)
  end
end

class UpdateChangeset < ROM::Changeset::Update
  map do |tuple|
    tuple.merge(updated: Time.now)
  end
end
