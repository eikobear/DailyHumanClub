class Messages < ROM::Relation[:sql]
  schema(infer: true) do
    associations do
      belongs_to :chatter
    end
  end

  def with_chatter
    join(chatters)
  end

end
