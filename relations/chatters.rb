class Chatters < ROM::Relation[:sql]
  schema(infer: true) do
    associations do
      belongs_to :chat
      belongs_to :user
    end
  end

  def with_chats
    join(chats)
  end

end
