class Users < ROM::Relation[:sql]
  
  schema(infer: true) do
    associations do
      has_many :chats, through: :chatters
      has_many :chatters
    end
  end

  def with_chatters
    join(chatters)
  end

end
