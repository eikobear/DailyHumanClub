require "sinatra"
require "open-uri"
require "json"

require "./database"

class DailyHumanClub < Sinatra::Base

  set :port, 8080

  d = Database.instance

  get "/" do
    return erb :index
  end

  post "/content/?" do
    @user = User[session[:id]]
    return erb :welcome_mat, layout: false unless @user

    @chat = @user.live_chat
    
    chat_duration = 86400.0 # seconds (86400 in a day)

    if @chat && ((Time.now - @chat.created_at) / chat_duration) > 1.0 # has a day gone by?
      @chat.live = false
      @chat.save
      @chat = nil
    end

    unless @chat
      other = User.other_than(@user.id).searching.pick_one
      if other
        Chat.db.transaction do
          @chat = Chat.new.save
          @my_chatter = @chat.add_chatter Chatter.new(user_id: @user.id)
          @their_chatter = @chat.add_chatter Chatter.new(user_id: other.id)
          #@their_chatter = d.chatter_repo.make other.id, @chat.id
          @user.searching = false
          other.searching = false
          @user.save
          other.save
          session[:partner_waiting] = other.id
        end
      else
        @user.searching = true
        @user.save
        return erb :searching, layout: false
      end
    end
   
    @end_time = @chat.created_at + chat_duration
    @my_chatter = @user.live_chat.chatters.select { |chatter| chatter.user_id == @user.id }.first
    @their_chatter = @user.live_chat.chatters.select { |chatter| chatter.user_id != @user.id }.first
    session[:chatter_id] ||= @my_chatter.id
    session[:their_id] ||= @their_chatter.user_id
    messages = Message.where(chatter_id: @chat.chatters.map(&:id)).order(:created_at).all
    chat_data = { participants: @chat.chatters, messages: messages }
    erb :chat, layout: false, locals: { chat: chat_data }
  end


  post "/signin/?" do

    open("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{params[:id_token]}") do |f|
      json = JSON.parse(f.read)
      session[:verified] = (json["email_verified"] == "true")
      session[:email] = json["email"]
      session[:lastname] = json["family_name"]
      session[:firstname] = json["given_name"]
      session[:avatar] = json["picture"]
    end

    #user = d.user_repo.lookup(session[:email]) || d.user_repo.make(email: session[:email], firstname: session[:firstname], lastname: session[:lastname], avatar: session[:avatar])
    user = User.where(email: session[:email]).first || User.new(email: session[:email], firstname: session[:firstname], lastname: session[:lastname], avatar: session[:avatar]).save
    session[:id] = user.id

    session[:avatar]
  end

  post "/signout/?" do
    session.clear
    200
  end


end

