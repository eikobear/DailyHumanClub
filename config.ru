if ENV["RACK_ENV"] == "development"
  ENV["DATABASE_URL"] = "sqlite://db/dev.sqlite"
  require "pry"
end

require "./dailyhumanclub"
require "./middlewares/chat_backend"

# a more cryptosecure rng:
require "sysrandom/securerandom"



Faye::WebSocket.load_adapter("thin")


ENV["SESSION_SECRET"] = SecureRandom.hex(64)
use Rack::Session::Cookie, key: "rack.session",
  path: "/",
  secret: ENV["SESSION_SECRET"]


use DHC::ChatBackend


run DailyHumanClub
