Sequel.migration do
  change do
    alter_table :chats do
      add_column :live, TrueClass, null: false, default: true
    end
  end
end

#today i learned that ruby doesn't have a Boolean class.
#it has a TrueClass and a FalseClass.
