Sequel.migration do
  change do

    create_table :chats do
      primary_key :id
      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end

    create_table :chatters do
      primary_key :id
      foreign_key :chat_id, :chats
      foreign_key :user_id, :users
      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end

    create_table :messages do
      primary_key :id
      foreign_key :chatter_id, :chatters
      column :text, String, null: false
      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end

  end
end
