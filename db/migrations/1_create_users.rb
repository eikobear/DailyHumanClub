Sequel.migration do
  change do
    create_table :users do
      primary_key :id
      column :email, String, null: false
      column :firstname, String, null: false
      column :lastname, String, null: false
      column :avatar, String, null: false
      column :nickname, String, null: true
      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end
  end
end
