Sequel.migration do
  change do
    alter_table :users do
      add_column :searching, TrueClass, null: false, default: false
    end
  end
end
