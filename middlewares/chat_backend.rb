require "faye/websocket"
require "json"
require "securerandom"

module DHC
  class ChatBackend
    KEEPALIVE_TIME = 15 #seconds

    def initialize app
      @app = app
      @clients = {}
      @db = Database.instance
      puts "ChatBackend initialized"
    end

    def call env
      req = Rack::Request.new env

      if Faye::WebSocket.websocket? env
        ws = Faye::WebSocket.new env, nil, { ping: KEEPALIVE_TIME }
        
        ws.on :open do |event|
          @clients[req.session[:id]] = ws if req.session[:id]
          ws
        end

        ws.on :message do |event|
          message = JSON.parse(event.data)
          if message["type"] == "message"
            message["text"] = Rack::Utils.escape_html(message["text"].gsub(/< *br *\/?>/, "\n"))
            m = Message.new(chatter_id: req.session[:chatter_id], text: message["text"]).save
            @clients[req.session[:their_id]].send(JSON.generate(message)) if m && req.session[:their_id] && @clients[req.session[:their_id]]
          end
          
        end

        ws.on :close do |event|
          @clients.delete(req.session[:id])
          ws = nil
        end

        ws.rack_response
      else
        response  = @app.call env
        
        req = Rack::Request.new env
        if req.session[:partner_waiting]
          json = JSON.generate type: "partner_found"
          ws = @clients[req.session[:their_id]]
          ws.send(json)
          req.session[:partner_waiting] = nil
        end
        response
      end
    end
    
  end
end
