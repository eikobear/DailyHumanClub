$(function() {
  $("#signout").hide();
  $("#avatar").hide();
  loadContent();
});

function initChat() {
  $("#input").keydown(sendMessage);
  $("#input").focus(function(e) {
    $("#input-placeholder").hide();
  });
  $("#input").blur(function(e) {
    if(e.target.textContent.length === 0) {
      $("#input-placeholder").show();
    }
  });
  //connectChat();
  scrollToBottom();
  startTimer();
}

function connectChat() {
  var scheme = "ws://";
  var uri = scheme + window.document.location.host + "/";
  ws = new WebSocket(uri);
  ws.onmessage = receiveMessage;
}

function receiveMessage(message) {
  var data = JSON.parse(message.data);
  if(data.type === "message") {
    addMessage(data.text, false);
  }
  else if(data.type === "partner_found") {
    loadContent();
  }
}

function addMessage(text, mine) {
  if(mine) {
    $("#chat").append("<div class=\"i-say\"><span>" + text + "</span></div>\n")
  }
  else {
    $("#chat").append("<div class=\"they-say\"><span>" + text + "</span></div>\n")
  }
  scrollToBottom();
}

function scrollToBottom() {
  $("#chat").stop().animate({
    scrollTop: $("#chat")[0].scrollHeight
  }, 500);
}

function onSignIn(googleUser) {
  var id_token = googleUser.getAuthResponse().id_token;

  $.post("/signin", {id_token: id_token })
    .done(function(data) {
      $("#avatar").html("<img src='" + data + "' />");
      loadContent();
    })
  .fail(function() {
    alert("failed to sign in.");
  });
  $("#signout").show();
  $("#avatar").show();
}

function loadContent() {
  $.post("/content")
    .done(function(data) {
      $("#content").html(data);
      if($(".websocket").length > 0){
        connectChat();
      }
      if($("#chat").length) {
        initChat();
      }
    })
    .fail(function() {
      alert("failed to load content.");
    });
}

function signOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('user signed out.');
  });
  $("#signout").hide();
  $("#avatar").hide();
  $.post("/signout").done(function(data) {
    loadContent();
  })
  .fail(function() {
    alert("failed to sign out.");
  });
}

function sendMessage(e) {
  if(e.which != 13 || e.shiftKey) {
    return;
  }

  e.preventDefault();
  
  if(e.target.textContent.length === 0) {
    return;
  }

  ws.send(JSON.stringify({ type: "message", text: e.target.innerHTML }));
  addMessage(e.target.innerHTML, true);
  e.target.innerHTML = "";

  /*
  $.post("/send_message", {message: e.target.innerHTML })
    .done(function(data) {
      e.target.innerHTML = "";
    })
  .fail(function() {
    alert("failed to send message!");
  });
  */

}

function startTimer() {
  var date = new Date($("#timeleft").text()).getTime();
  var updateTimer = function() {
    var now = new Date().getTime();
    var distance = date - now;
    if (distance < 0) {
      distance = 0
    }
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    $("#timeleft").text(pad(hours, 2) + ":" + pad(minutes, 2) + ":" + pad(seconds, 2));
  };
  updateTimer();
  var timer = setInterval(updateTimer, 1000);
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
