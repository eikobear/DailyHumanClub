
class Chat < Sequel::Model
  many_to_many :users, join_table: :chatters
  one_to_many :chatters
end

