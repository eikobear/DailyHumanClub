
class User < Sequel::Model
  many_to_many :chats, join_table: :chatters
  one_to_many :chatters

  def live_chat
    chats_dataset.where(live: true).first
  end

  dataset_module do
    def other_than id
      exclude(id: id)
    end

    def searching
      where(searching: true)
    end

    def pick_one
      order { random.function }.limit(1).first
    end
  end

end

