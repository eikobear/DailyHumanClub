
class Chatter < Sequel::Model
  many_to_one :chat
  many_to_one :user
  one_to_many :messages
end

